var Mode = {
	ENROLLMENT:"enrollment",
	CHALLENGE:"challenge",
	DEBUG:"debug"
}

var Config = {
	mode 				: Mode.ENROLLMENT,
	size 				: 30,
    spacing 			: 1,
    mode 				: true,
    fadeDelay 			: 250,
    fadingOutSteps 		: 20,
    inactivityDelay 	: 2000,
    totalSteps 			: 3,
    backgroundColor 	: [0,0,0],
    squareColor 		: [255,255,255],
    fps 				: 1000/60,
    autoEndTrajectory   : true,
    timestampSpacing	: 12,

    //These will be updated dynamically from SignaturePlugin.js
    nbRows				: 0,
    nbCols 				: 0,
    width 				: 0,
    height				: 0,
    locale              : [
        {
            title           : "Draw your gesture",
            subTitle        : "Tip: to improve accuracy, try drawing at a different position of the screen."
        },
        {
            title           : "One more time",
            subTitle        : "Tip: you can draw more than one stroke."
        },
        {
            title           : "One last time",
            subTitle        : "Tip: curves are harder to reproduce than lines."
        }
    ]
};