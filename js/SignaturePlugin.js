var SignaturePlugin = function() {
    //Elements
    var _root 					= null;
    var _canvas                 = null;
    var _context                = null;

    //Points
    var _visiblePoints          = {};
    var _fadingPoints           = {}; //objects instead of arrays, faster for lookup
    var _gesturePoints          = [];
    var _previousGestures       = [];
    var _lastPt                 = null; //object {x:int, y:int}

    //intervals
    var _onLoopInterval         = 0;

    //UI Classes
    var _background             = null;
    var _instructions           = null;
    var _progressBar			= null;

    //Status
    var _step                   = 0;

    //Add events
    setEvents(true);

    _root = document.getElementById('canvas');
    _root.id = "root";

    _background = new Background();
    _root.appendChild(_background.getView());

    //create the canvas
    _canvas = document.createElement('canvas');
    _canvas.id = "canvas_grid";
    _root.appendChild(_canvas);
    _context = _canvas.getContext("2d");

    _instructions = new Instructions();
    _root.appendChild(_instructions.getView());

    _progressBar = new ProgressBar();
    _progressBar.getView().addEventListener("completed", onProgressBarCompleted, false);
    _root.appendChild(_progressBar.getView());

    document.body.style.backgroundColor = ColorUtils.RGBToHex(Config.backgroundColor);

    window.addEventListener("resize", resize, false);
    resize(); //resize once .. usually called once QML is ready
    setMode(Mode.ENROLLMENT);
    

    function onProgressBarCompleted($event) {
    	setEvents(false);
        _step++;
        if((_step) == (Config.totalSteps) || Config.mode == Mode.CHALLENGE || Config.mode == Mode.DEBUG) {
           reset(true);
           setMode(Mode.CHALLENGE);
           _instructions.hide();
        } else {
            //training step completed
            flash(); 
        }
    }

    function setEvents(val) {
    	document.removeEventListener("mousedown",   onTouchDown,    false);
       	document.removeEventListener("mousemove",   onTouchMove,    false);
       	document.removeEventListener("mouseup",     onTouchUp,      false);

       	if(val) {
           	document.addEventListener("mousedown",   	onTouchDown,    false);
           	document.addEventListener("mousemove",   	onTouchMove,    false);
           	document.addEventListener("mouseup",     	onTouchUp,      false);
        }
    }

    function toggleRectangle($index, $on) {
        if($on) {
            _visiblePoints[$index] = getTime();
        } else {
            delete _visiblePoints[$index];
        }

        var pt = indexToPoint($index);
        var positionPoint = pointToPosition(pt);

        _context.fillStyle = $on ? ColorUtils.RGBToHex(Config.squareColor): ColorUtils.RGBToHex(Config.backgroundColor);
        _context.fillRect(positionPoint.x, positionPoint.y, Config.size, Config.size);
        _context.lineWidth = 1;
        _context.strokeStyle = ColorUtils.RGBToHex(Config.backgroundColor);
        _context.strokeRect(positionPoint.x-(Config.spacing/2), positionPoint.y-(Config.spacing/2), Config.size+Config.spacing, Config.size+Config.spacing);
    }

    function resize(e) {
    	Config.width = _canvas.width = window.innerWidth;
    	Config.height = _canvas.height = window.innerHeight;

        Config.nbRows = Math.ceil(Config.height / (Config.size + Config.spacing));
        Config.nbCols = Math.ceil(Config.width / (Config.size + Config.spacing));

        if(_background) _background.resize();

        reset(false);
    }

    function reset($soft) {
        if(!$soft) {
            _step = 0;
            _previousGestures = [];
        } else {
            _previousGestures.push(_gesturePoints);
        }

        setEvents(true); //make sure we get the events back

        _visiblePoints = {};
        _fadingPoints = {};
        _gesturePoints = [];
        _lastPt = null;

        _context.clearRect (0, 0, Config.width, Config.height);
    }

    function setMode(mode) {
        Config.mode = mode;

        window.clearInterval(_onLoopInterval);
       	_onLoopInterval = 0;

        if(Config.mode == Mode.CHALLENGE) {
            _onLoopInterval = window.setInterval(onLoop, 1000/60);
        } else if(Config.mode == Mode.ENROLLMENT) {
        	_instructions.show();
        }
    }

    function onLoop() {
        //check for new points to start fading out
        var time = getTime();
        var pointsToFade = [];
        for(var key in _visiblePoints) {
            if(_visiblePoints[key] < (time - Config.fadeDelay)) {
                //remove the point from the visible point list
                delete _visiblePoints[key];

                //add the point to the list of points to be fade out
                _fadingPoints[key] = 0;
            }
        } 

        //check if the fading point is now back into the visible point
        for(var key in _fadingPoints) {
            if(_visiblePoints[key]) {
                delete _fadingPoints[key];
            }
        }

        //fading out the points
        var dr = (Config.backgroundColor[0] - Config.squareColor[0]) / Config.fadingOutSteps;
        var dg = (Config.backgroundColor[1] - Config.squareColor[1]) / Config.fadingOutSteps;
        var db = (Config.backgroundColor[2] - Config.squareColor[2]) / Config.fadingOutSteps;

        var dr2 = (Config.squareColor[0] - Config.backgroundColor[0]) / Config.fadingOutSteps;
        var dg2 = (Config.squareColor[1] - Config.backgroundColor[1]) / Config.fadingOutSteps;
        var db2 = (Config.squareColor[2] - Config.backgroundColor[2]) / Config.fadingOutSteps;

        var i, pt, pos, alpha;
        for(key in _fadingPoints) {
            i = _fadingPoints[key];
            pt = indexToPoint(key);
            alpha = 1 - (i / Config.fadingOutSteps);
            pos = pointToPosition(pt);
            _context.fillStyle = 'rgba(' + Math.round(Config.squareColor[0] + dr * i) + ',' + Math.round(Config.squareColor[1] + dg * i) + ',' + Math.round(Config.squareColor[2] + db * i) + ',' + alpha + ')';
            _context.clearRect(pos.x-(Config.spacing/2), pos.y-(Config.spacing/2), Config.size+Config.spacing+(Config.spacing/2), Config.size+Config.spacing+(Config.spacing/2));
            _context.fillRect(pos.x, pos.y, Config.size, Config.size);

            _fadingPoints[key]++;
            if(_fadingPoints[key] > Config.fadingOutSteps) {
                delete _fadingPoints[key];
            }
        }
    }

    function onTouchDown($event) {
    	var pos = getPositionFromEvent(event);
        _lastPt = positionToPoint(pos);
        var index = pointToIndex(_lastPt);
        _gesturePoints.push([pos]);
        toggleRectangle(pointToIndex(_lastPt), true);    
    }

    function getPositionFromEvent($event) {
    	var pos = {
            x: $event.x,
            y: $event.y,
            t: getTime()
        };

    	if(pos.x < 0) pos.x = 0;
    	if(pos.y < 0) pos.y = 0;
    	if(pos.x > Config.width) pos.x = Config.width;
    	if(pos.y > Config.height) pos.y = Config.height;

    	return pos;
    }

    function getTime() {
    	return (new Date().getTime() + '');
    }

    function flash() {
    	var thisClass = this;
    	setEvents(false);
    	_canvas.className = "hide";
        _canvas.addEventListener( 'webkitTransitionEnd', flashCompleted, false);

        _instructions.setStep(_step);
    }

    function flashCompleted() {
    	_canvas.removeEventListener("webkitTransitionEnd", flashCompleted, false);
    	reset(true);
    	_canvas.className = "";
    	setEvents(true);
    }

    function onTouchUp($event) {
    	if(Config.autoEndTrajectory && _step>0) {
    		if(_previousGestures[_previousGestures.length-1].length == _gesturePoints.length) {
            	if(_step < Config.totalSteps-1) {
                	onProgressBarCompleted();
                } else {
                    onProgressBarCompleted();
                }
            }
        } else if(_lastPt != null) {
            _progressBar.start();
        }

        _lastPt = null;
    }

    function onTouchMove($event) {
        if(_lastPt == null) return;

        var pos = getPositionFromEvent($event);

        //check if the last point were captured more than 12ms ago
        if(_gesturePoints[_gesturePoints.length-1].length> 0) {
        	var ptTimestamp = parseInt(pos.t);
        	var lastPtTimestamp = parseInt(_gesturePoints[_gesturePoints.length-1][_gesturePoints[_gesturePoints.length-1].length-1].t);
        	
        	if(ptTimestamp - lastPtTimestamp < Config.timestampSpacing) {
        		return;
        	}
        }

        _gesturePoints[_gesturePoints.length-1].push(pos);

        var pt = positionToPoint(pos);
        var pts = getPointBetweenPoints(_lastPt, pt);

        for(var i=0; i<pts.length; i++) {
            toggleRectangle(pointToIndex(pts[i]), true);
        }

        _lastPt = pt;
    }

    function getPointBetweenPoints($firstPt, $secondPt) {
        var points = [];
        var x1 = $firstPt.x;
        var y1 = $firstPt.y;
        var x2 = $secondPt.x;
        var y2 = $secondPt.y;

        var i; // loop counter
        var ystep, xstep; // the step on y and x axis
        var error; // the error accumulated during the increment
        var errorprev; // *vision the previous value of the error variable
        var y = y1, x = x1; // the line points
        var ddy, ddx; // compulsory variables: the double values of dy and dx
        var dx = x2 - x1;
        var dy = y2 - y1;
        points.push({x:x1, y:y1});
        // NB the last point can't be here, because of its previous point (which has to be verified)
        if (dy < 0) {
            ystep = -1;
            dy = -dy;
        } else
            ystep = 1;
        if (dx < 0) {
            xstep = -1;
            dx = -dx;
        } else
            xstep = 1;
        ddy = 2 * dy; // work with double values for full precision
        ddx = 2 * dx;
        if (ddx >= ddy) { // first octant (0 <= slope <= 1)
            // compulsory initialization (even for errorprev, needed when dx==dy)
            errorprev = error = dx; // start in the middle of the square
            for (i = 0; i < dx; i++) { // do not use the first point (already done)
                x += xstep;
                error += ddy;
                if (error > ddx) { // increment y if AFTER the middle ( > )
                    y += ystep;
                    error -= ddx;
                    // three cases (octant == right->right-top for directions below):
                    if (error + errorprev < ddx) { // bottom square also
                        points.push({x:x, y:y - ystep});
                    } else if (error + errorprev > ddx) { // left square also
                        points.push({x:x - xstep, y:y});
                    } else { // corner: bottom and left squares also
                        points.push({x:x, y:y - ystep});
                        points.push({x:x - xstep, y:y});
                    }
                }
                points.push({x:x, y:y});
                errorprev = error;
            }
        } else { // the same as above
            errorprev = error = dy;
            for (i = 0; i < dy; i++) {
                y += ystep;
                error += ddx;
                if (error > ddy) {
                    x += xstep;
                    error -= ddy;
                    if (error + errorprev < ddy)
                        points.push({x:x - xstep, y:y});
                    else if (error + errorprev > ddy)
                        points.push({x:x, y:y - ystep});
                    else {
                        points.push({x:x - xstep, y:y});
                        points.push({x:x, y:y - ystep});
                    }
                }
                points.push({x:x, y:y});
                errorprev = error;
            }
        }

        points.push({x:x2, y:y2});

        return points;
    }

    function indexToPoint($index) {
        var pt = {};
        pt.y = Math.floor(($index) / Config.nbCols);
        pt.x = Math.ceil(($index) - (pt.y * Config.nbCols));    
        return pt;
    }

    function pointToIndex($pt) {
        var index = ($pt.y * (Config.nbCols)) + $pt.x;
        return index;
    }

    function positionToPoint($pos) {
        var pt = {};
        pt.x = Math.floor($pos.x / (Config.size + Config.spacing));
        pt.y = Math.floor($pos.y / (Config.size + Config.spacing));
        return pt;
    }

    function pointToPosition($pt) {
        var pos = {};
        pos.x = Math.floor($pt.x * (Config.size + Config.spacing));
        pos.y = Math.floor($pt.y * (Config.size + Config.spacing));
        return pos;
    }
}