var Background = function() {
    var _canvas;
    var _context;

    _canvas = document.createElement('canvas');
    _canvas.id = "canvas_background";
    _context = _canvas.getContext("2d");
    
    this.resize = function() {
        _canvas.width = Config.width;
        _canvas.height = Config.height;

        drawGrid();
        drawVignette();
    }

    function drawVignette() {
        //draw the gradient
        var grad = _context.createRadialGradient(_canvas.width/2,_canvas.height/2,0,_canvas.width/2,_canvas.height/2, _canvas.width/1.2);
        grad.addColorStop(0, 'rgba(0,0,0,0.6)');
        grad.addColorStop(0.75, 'rgba(0,0,0,1)');
        _context.fillStyle = grad;
        _context.fillRect(0, 0, _canvas.width,_canvas.height);
    }

    this.getView = function() {
        return _canvas;
    }
    
    function drawGrid() {
        _context.beginPath();
        _context.lineWidth = 1; 
        _context.strokeStyle = '#FFFFFF';

        //rows
        var pos;
        for(var row=0; row<Config.nbRows; row++) {
            pos = row*(Config.size + Config.spacing) - 0.5
            _context.moveTo(0,pos);
            _context.lineTo(_canvas.width, pos);
        }

        //cols
        for(var col=0; col<Config.nbCols; col++) {
            pos = col*(Config.size + Config.spacing) - 0.5;
            _context.moveTo(pos, 0);
            _context.lineTo(pos, _canvas.height);
        }

        _context.stroke();
    }
}