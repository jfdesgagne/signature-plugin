var Instructions = function() {
	var _currentNumber = Config.countDown;	
	var _titleText = null;
	var _subTitleText = null;
	var _root = null;
	var that = this;
	
	var _locale = Config.locale;
	var _step = 0;

	init();

	function init() {
		_root = document.createElement('div');
		_root.id = "instructions";

		_titleText = document.createElement('h1');
		_root.appendChild(_titleText);

		_subTitleText = document.createElement('h2');
		_root.appendChild(_subTitleText);

		updateText();
	}

	function updateText() {
		if(_titleText) {
			_titleText.innerHTML = _locale[_step].title;
			_subTitleText.innerHTML = _locale[_step].subTitle;
		}
	}

	this.setLocale = function($locale) {
		_locale = $locale;
	}

	this.setStep = function($step) {
		_step = $step;
		this.hide(true);
	}

	this.hide = function(showAgain) {
		console.log("Instructions::hide() showAgain:", showAgain);
		_root.className = "";
		if(showAgain) {
			console.log("adding listener")
			_root.addEventListener('webkitTransitionEnd', that.show, false);
		}
	}

	this.show = function() {
		updateText();
		_root.removeEventListener( 'webkitTransitionEnd', that.show, false);
		_root.className = "show";
	}

	this.reset = function() {
		_step = 0;
		updateText();	
	}

	this.getView = function() {
        return _root;
    }
}