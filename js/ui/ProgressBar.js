var ProgressBar = function() {
	var _bg = null;
	var _bar = null;
	var _endTimeout = 0;
	var that = this;

	_bg = document.createElement('div');
	_bg.id = 'progress';
	
	_bar = document.createElement('div');
	_bar.id = 'progress_bar';
	_bg.appendChild(_bar);
	

	function setEvents(val) {
		if(val) {
			if(Config.isMobile) {
	            document.addEventListener("touchstart",  stopAnimations,	true);    
	        } else {
	           document.addEventListener("mousedown",   stopAnimations,    	true);
	        }
	    } else {
	    	if(Config.isMobile) {
	            document.removeEventListener("touchstart",  stopAnimations,	true);     
	        } else {
	           document.removeEventListener("mousedown",   stopAnimations,  true);
	        }
	    }
	}

	function stopAnimations() {
		_bar.style.setProperty("-webkit-transition", "");
		_bg.className = "hide";
		clearTimeout(_endTimeout);
	}

	this.getView = function() {
		return _bg;
	}

	this.end = function() {
		stopAnimations();
		setEvents(false);

		var evt = document.createEvent("Event");
			evt.initEvent("completed",true,true); 
		_bg.dispatchEvent(evt);	
	}

	this.start = function() {
		_bg.className = "show";
		_bar.style.setProperty("-webkit-transition", "width " + Config.inactivityDelay + "ms linear");
		_endTimeout = setTimeout(function() {that.end();}, Config.inactivityDelay);
		setEvents(true);
	}
}