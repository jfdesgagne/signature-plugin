var ColorUtils = {
	hexToRGB:function($hex) {
		var h = ($hex.charAt(0)=="#") ? $hex.substring(1,7):$hex;
		var r = parseInt(h.substring(0,2),16);
		var g = parseInt(h.substring(2,4),16);
		var b = parseInt(h.substring(4,6),16);
		return [r,g,b];
	},
	RGBToHex:function($rgb) {
		var toHex = function(n) {
			n = parseInt(n,10);
			if (isNaN(n)) return "00";
			n = Math.max(0,Math.min(n,255));
			return "0123456789ABCDEF".charAt((n-n%16)/16) + "0123456789ABCDEF".charAt(n%16);
		}

		return '#' + toHex($rgb[0]) + toHex($rgb[1]) + toHex($rgb[2]);
	}
}